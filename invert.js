function getSwap(Obj) {
  if (!Obj || Array.isArray(Obj)) {
    return [];
  } else {
    let result = {};
    let value;
    for (let key in Obj) {
      value = Obj[key];
      value = JSON.stringify(value);
      result[value] = key;
    }
    return result;
  }
}

module.exports = getSwap;
