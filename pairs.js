function getPair(Obj) {
  if (!Obj || Array.isArray(Obj)) {
    return [];
  } else {
    let storeInArrayFormat = [];
    for (let key in Obj) {
      storeInArrayFormat.push(key, Obj[key]);
    }
    let result = [];
    while (storeInArrayFormat.length > 0) {
      const chunk = storeInArrayFormat.splice(0, 2);
      result.push(chunk);
    }
    return result;
  }
}

module.exports = getPair;
