let displayData = require("../pairs");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

let result = displayData(testObject);
console.log(result);

const testObject1 = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
  arr: [1, 2, 3],
};

let testResult2 = displayData(testObject1);
console.log(testResult2);

const testObject3 = [];

let testResult3 = displayData(testObject3);
console.log(testResult3);
