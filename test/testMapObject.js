let DisplayMapObject = require("../mapObject");

let testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
let result = DisplayMapObject(testObject, (value, key) => {
  return value + 2;
});
console.log(result);

let testObject1 = [1, 234, 2];
let result1 = DisplayMapObject(testObject1, (value, key) => {
  return value + 2;
});
console.log(result1);
