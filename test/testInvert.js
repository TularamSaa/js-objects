let displayData = require("../invert");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
let result = displayData(testObject);
console.log(result);

const testObject1 = [];
let testResult = displayData(testObject1);
console.log(testResult);

const testObject2 = { a: { b: 1 } };
let testResult1 = displayData(testObject2);
console.log(testResult1);
