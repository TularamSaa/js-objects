let displayData = require("../values");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
console.log(displayData.getValues(testObject));

const testArray = [1, 2, 3];
console.log(displayData.getValues(testArray));

const testForEmpty = {};
console.log(displayData.getValues(testForEmpty));
