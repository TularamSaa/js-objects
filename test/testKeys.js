let displayData = require("../keys");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
console.log(displayData.getkeys(testObject));

const testArray = [1, 2, 3];
console.log(displayData.getkeys(testArray));

const testForEmpty = {};
console.log(displayData.getkeys(testForEmpty));
