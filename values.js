function getValues(Obj) {
  if (!Obj || Array.isArray(Obj)) {
    return [];
  } else {
    result = [];
    for (let val in Obj) {
      result.push(Obj[val]);
    }
  }
  return result;
}

module.exports = { getValues };
