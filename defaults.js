function getDefaults(Obj, defaultProps) {
  if (typeof Obj != "object" || Array.isArray(defaultProps)) {
    return [];
  } else {
    for (let prop in defaultProps) {
      // console.log(Obj[prop]);
      if (Obj.hasOwnProperty(prop) === false) {
        Obj[prop] = defaultProps[prop];
      }
    }
  }
  return Obj;
}

module.exports = getDefaults;
