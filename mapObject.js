function getMapObject(obj, cb) {
  if (obj.constructor !== Object) {
    return {};
  }
  const object = {};
  for (let prop in obj) {
    object[prop] = cb(obj[prop]);
  }
  return object;
}

module.exports = getMapObject;
